<?php declare(strict_types = 1);

require_once __DIR__ . '/../bootstrap.php';

$builder = new \Echo511\CypherFluent\StatementBuilder();

\Tester\Assert::same(
	'MATCH (s)-[r:`rl` {"name":"John"} ], (o) CREATE (p:`AHA`) DELETE o RETURN s, r',
	$builder->statement(
		$builder->match(
			$builder->concat(
				$builder->node('s'),
				$builder->direction(),
				$builder->relationship('r', 'rl', [
					'name' => 'John'
				])
			),
			$builder->node('o')
		),
		$builder->create(
			$builder->node('p', 'AHA')
		),
		$builder->delete('o'),
		$builder->return('s', 'r')
	)->build()
);
