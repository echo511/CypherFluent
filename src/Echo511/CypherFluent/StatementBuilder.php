<?php declare(strict_types = 1);

namespace Echo511\CypherFluent;

class StatementBuilder
{


	public function statement(...$params)
	{
		return new Statement(...$params);
	}


	public function create(...$params)
	{
		return new \Echo511\CypherFluent\StatementParts\CommandStatementPart('CREATE', ...$params);
	}


	public function match(...$params)
	{
		return new \Echo511\CypherFluent\StatementParts\CommandStatementPart('MATCH', ... $params);
	}


	public function return(...$params)
	{
		return new \Echo511\CypherFluent\StatementParts\CommandStatementPart('RETURN', ...$params);
	}


	public function delete(...$params)
	{
		return new \Echo511\CypherFluent\StatementParts\CommandStatementPart('DELETE', ...$params);
	}


	public function concat(...$params)
	{
		return new \Echo511\CypherFluent\StatementParts\Concatenate('', ...$params);
	}


	public function node(string $designation, ?string $label = NULL, $properties = [])
	{
		return new \Echo511\CypherFluent\StatementParts\Node($designation, $label, $properties);
	}


	public function direction($direction = \Echo511\CypherFluent\StatementParts\Direction::NONE)
	{
		return new \Echo511\CypherFluent\StatementParts\Direction($direction);
	}


	public function relationship(string $designation, string $label, array $properties = [])
	{
		return new \Echo511\CypherFluent\StatementParts\Relationship($designation, $label, $properties);
	}


}
