<?php declare(strict_types = 1);

namespace Echo511\CypherFluent\StatementParts;

class Concatenate implements \Echo511\CypherFluent\StatementPartInterface
{

	/**
	 * implode() delimiter
	 * @var string
	 */
	private $delimiter;

	/**
	 * @var \Echo511\CypherFluent\StatementPartInterface[]
	 */
	private $statementParts = [];


	public function __construct(string $delimiter, ...$parts)
	{
		$this->delimiter = $delimiter;
		$this->statementParts = $parts;
	}


	public function addPart(\Echo511\CypherFluent\StatementPartInterface $part)
	{
		$this->statementParts[] = $part;
	}


	public function __toString()
	{
		return \implode($this->delimiter, $this->statementParts);
	}


}
