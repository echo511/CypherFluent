<?php declare(strict_types = 1);

namespace Echo511\CypherFluent\StatementParts;

class Direction implements \Echo511\CypherFluent\StatementPartInterface
{

	const NONE = 'none';
	const LEFT = 'left';
	const RIGHT = 'right';

	private $direction;


	public function __construct($direction)
	{
		$this->direction = $direction;
	}


	public function __toString()
	{
		if ($this->direction === self::LEFT) {
			return '<-';
		} elseif ($this->direction === self::RIGHT) {
			return '->';
		} else {
			return '-';
		}
	}


}
