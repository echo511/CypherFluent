<?php declare(strict_types = 1);

namespace Echo511\CypherFluent\StatementParts;

class Node implements \Echo511\CypherFluent\StatementPartInterface
{


	private $designation;

	private $label;

	private $properties;


	public function __construct(string $designation = '', ?string $label = NULL, array $properties = [])
	{
		$this->designation = $designation;
		$this->label = $label;
		$this->properties = $properties;
	}


	public function __toString()
	{
		$encoded = $this->properties ? ' ' . \json_encode((object) $this->properties) . ' ' : '';
		$label = $this->label ? ':`' . $this->label . '`' : '';
		return '(' . $this->designation . $label . $encoded . ')';
	}


}
