<?php declare(strict_types = 1);

namespace Echo511\CypherFluent\StatementParts;

class CommandStatementPart
{

	/**
	 * MATCH || CREATE || RETURN || ...
	 * @var string
	 */
	private $name;

	/**
	 * @var \Echo511\CypherFluent\StatementPartInterface[]
	 */
	private $statementParts = [];


	public function __construct(string $name, ... $parts)
	{
		$this->name = $name;
		$this->statementParts = $parts;
	}


	public function __toString()
	{
		return $this->name . ' ' . (new Concatenate(', ', ...$this->statementParts));
	}


}
