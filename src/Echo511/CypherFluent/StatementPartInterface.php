<?php declare(strict_types = 1);

namespace Echo511\CypherFluent;

interface StatementPartInterface
{


	public function __toString();


}
