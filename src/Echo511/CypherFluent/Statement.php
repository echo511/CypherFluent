<?php declare(strict_types = 1);

namespace Echo511\CypherFluent;

class Statement extends \Echo511\CypherFluent\StatementParts\Concatenate
{


	public function __construct(...$parts)
	{
		parent::__construct(' ', ...$parts);
	}


	public function build()
	{
		return (string) $this;
	}


}
