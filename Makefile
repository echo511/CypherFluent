cs:
	- vendor/bin/phpcs --standard=phpcs.source.xml src/

cs-fix:
	- vendor/bin/phpcbf --standard=phpcs.source.xml src/

phpstan:
	- vendor/bin/phpstan analyse -l 7 -c phpstan.neon src/

run-tests:
	- vendor/bin/tester tests/
